### mlops-zoomcamp-homework

Homework for https://github.com/DataTalksClub/mlops-zoomcamp 2022 course

## Week 1: Introduction

    What is MLOps
    MLOps maturity model
    Running example: NY Taxi trips dataset
    Why do we need MLOps
    Course overview
    Environment preparation
    Homework : ---> vanilla classifier training on NY Taxi trips dataset


## Week 2:  Experiment tracking and model management

    Experiment tracking intro
    Getting started with MLflow
    Experiment tracking with MLflow
    Model management
    Model registry
    MLflow in practice
    Homework : ---> MLflow project